<?php

namespace App\Http\Middleware;

use Closure;

class ResponseTimeMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $response = $next($request);

        $time = (microtime(true) - $_SERVER['REQUEST_TIME_FLOAT']) * 1000;

        $response->header('X-Response-Time-Ms', number_format($time, 3));

        return $response;
    }
}
