<?php declare(strict_types=1);

use Illuminate\Http\Request;
use App\FeedGenerator;
use FeedIo\FeedIo;

$router->get('/', function (Request $request, FeedGenerator $feed) {
    
    // Seed off of query string for deterministic feed generation
    $seed = crc32(md5($request->getQueryString()));
    mt_srand($seed);

    $feed->setTitle($request->input('feed_title'));
    $feed->setItemCount((int)$request->input('num_items'));
    $feed = $feed->generateFeed($feed);

    switch ($request->input('type')) {
        case 'rss':
            return response(
                $feed->export('rss', true),
                200,
                ['Content-Type' => 'application/rss+xml']
            );
        case 'atom':
            return response(
                $feed->export('atom', true),
                200,
                ['Content-Type' => 'application/atom+xml']
            );
        default:
            throw new \UnexpectedValueException("Unknown type");
    }
});
