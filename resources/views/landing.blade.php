<?php

$control = function (string $label, string $id, string $control, string $help = null) {
    $template = '
        <div class="field is-horizontal">
            <div class="field-label is-normal">
                <label class="label" for="%s">%s</label>
            </div>
            <div class="field-body">
                <div class="control">
                    %s
                </div>
            </div>
        </div>
    ';

    if ($help) {
        $control .= sprintf('<p class="help">%s</p>', $help);
    }

    return sprintf($template, $id, $label, $control);
};

$radio = function (string $label, string $name, array $options, $default = null, string $help = null) use ($control): string {
    $template = '
        <label class="radio">
            <input type="radio" name="%s" id="%s" value="%s" %s>
            %s
        </label>
    ';

    $output = '';
    foreach ($options as $display => $value) {
        $checked = $default == $value ? 'checked' : '';
        $output .= sprintf($template, $name, $name, $value, $checked, $display);
    }

    return $control($label, $name, $output, $help);
};

$text = function (string $label, string $name, string $default = null, string $help = null) use ($control): string {
    $template = '
        <input class="input" type="string" name="%s" id="%s" value="%s">
    ';

    $output = sprintf($template, $name, $name, $default);

    return $control($label, $name, $output, $help);
};

$select = function (string $label, string $name, array $options, $default = null, string $help = null) use ($control): string {
    $select_template = '
        <select name="%s" id="%s">
            %s
        </select>
    ';
    $option_template = '
        <option value="%s" %s>%s</option>
    ';

    $output = '';

    foreach ($options as $display => $value) {
        $selected = $default == $value ? 'selected' : '';
        $output .= sprintf($option_template, $value, $selected, $display);
    }

    $output = sprintf($select_template, $name, $name, $label, $output);

    return $control($label, $name, $output, $help);
};

$number = function (string $label, string $name, string $default = null, string $help = null) use ($control): string {
    $template = '
        <input class="input" type="number" name="%s" id="%s" value="%s">
    ';

    $output = sprintf($template, $name, $name, $default);

    return $control($label, $name, $output, $help);
};

$submit = function ($label) {
    $template = '
        <div class="field is-horizontal">
            <div class="field-label">
                <!-- Left empty for spacing -->
            </div>
            <div class="field-body">
                <div class="field">
                <div class="control">
                    <button class="button is-primary">%s</button>
                </div>
                </div>
            </div>
        </div>
    ';

    return sprintf($template, $label);
};

?>
<!doctype HTML>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>{{ env('APP_NAME', 'Example Feed Generator') }}</title>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.7.2/css/bulma.min.css">
        <style>
            .field-body { flex-grow: 4; }
        </style>
    </head>
    <body>
        <section class="section">
            <div class="container">
                <h1 class="title">{{ env('APP_NAME', 'Example Feed Generator') }}</h1>

                <form method="get" action="/v1/">
                    <?php

                    $options = [
                        'RSS' => 'rss',
                        'Atom' => 'atom',
                    ];
                    echo $select('Type', 'type', $options, 'rss');

                    $help = 'If not set, a title will be generated automatically.';
                    echo $text('Feed Title', 'feed_title', '', $help);
                    
                    $help = 'If yes, podcast enclosures will be included (along with iTunes tags)';
                    echo $radio('Enclosures', 'enclosure', ['Yes' => 1, 'No' => 0], 0, $help);

                    $help = 'If no, sets itunes:block, adds a noindex header, etc.';
                    echo $radio('Publish', 'publish', ['Yes' => 1, 'No' => 0], 1, $help);

                    echo $number('Number of Items', 'num_items', 20);

                    echo $submit('Generate');
                    
                    ?>
                </form>
            </div>
        </section>
    </body>
</html>
