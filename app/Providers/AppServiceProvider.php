<?php

namespace App\Providers;

use Faker;
use FeedIo;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register(): void
    {
        $this->app->singleton(Faker\Generator::class, function (): Faker\Generator {
            return Faker\Factory::create();
        });
    }
}
