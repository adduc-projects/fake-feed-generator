<?php declare(strict_types=1);

namespace App;

use Faker\Generator;
use Zend\Feed\Writer\Feed;

class FeedGenerator
{
    /** @var Generator */
    protected $faker;

    /** @var Feed */
    protected $feed;

    /** @var ?string */
    protected $title = null;

    /** @var int */
    protected $item_count = 20;

    public function __construct(Generator $generator, Feed $feed)
    {
        $this->faker = $generator;
        $this->feed = $feed;
    }

    public function setTitle(?string $title): void
    {
        $this->title = $title;
    }

    public function getTitle(): string
    {
        $title = $this->title ?: "{$this->faker->name}'s Podcast";
        return "[FAKE] {$title}";
    }

    public function getDescription()
    {
        return "[FAKE] {$this->faker->sentence}";
    }

    public function getItemTitle(): string
    {
        return "[FAKE] {$this->faker->name}";
    }

    public function setItemCount(int $item_count): void
    {
        $this->item_count = $item_count;
    }

    public function generateFeed($type): Feed
    {
        $this->feed->setTitle($this->getTitle());

        foreach (range(0, $this->item_count) as $i) {
            $item = $this->feed->createEntry();

            $item->setTitle($this->getItemTitle());

            $this->feed->addEntry($item);
        }
        
        return $this->feed;
    }
}
